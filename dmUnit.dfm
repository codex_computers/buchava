object dm: Tdm
  OldCreateOrder = False
  Height = 792
  Width = 560
  object tblMerniMesta: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BUC_MERNI_MESTA'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    SKRATEN_NAZIV = :SKRATEN_NAZIV,'
      '    MESTO = :MESTO,'
      '    NIVO_BUCAVA_D = :NIVO_BUCAVA_D,'
      '    NIVO_BUCAVA_V = :NIVO_BUCAVA_V,'
      '    NIVO_BUCAVA_N = :NIVO_BUCAVA_N,'
      '    STEPEN = :STEPEN,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    BUC_MERNI_MESTA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO BUC_MERNI_MESTA('
      '    ID,'
      '    NAZIV,'
      '    SKRATEN_NAZIV,'
      '    MESTO,'
      '    NIVO_BUCAVA_D,'
      '    NIVO_BUCAVA_V,'
      '    NIVO_BUCAVA_N,'
      '    STEPEN,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :SKRATEN_NAZIV,'
      '    :MESTO,'
      '    :NIVO_BUCAVA_D,'
      '    :NIVO_BUCAVA_V,'
      '    :NIVO_BUCAVA_N,'
      '    :STEPEN,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select b.id,'
      '       b.naziv,'
      '       b.skraten_naziv,'
      '       b.mesto,'
      '       m.naziv as mesto_naziv,'
      '       o.naziv as opstina_naziv,'
      '       b.nivo_bucava_d, b.nivo_bucava_v, b.nivo_bucava_n,'
      '       b.stepen,'
      '       b.custom1,'
      '       b.custom2,'
      '       b.custom3,'
      '       b.ts_ins,'
      '       b.ts_upd,'
      '       b.usr_ins,'
      '       b.usr_upd'
      'from buc_merni_mesta b'
      'inner join mat_mesto m on m.id = b.mesto'
      'left outer join mat_opstina o on o.id = m.opstina'
      ''
      ' WHERE '
      '        B.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select b.id,'
      '       b.naziv,'
      '       b.skraten_naziv,'
      '       b.mesto,'
      '       m.naziv as mesto_naziv,'
      '       o.naziv as opstina_naziv,'
      '       b.nivo_bucava_d, b.nivo_bucava_v, b.nivo_bucava_n,'
      '       b.stepen,'
      '       b.custom1,'
      '       b.custom2,'
      '       b.custom3,'
      '       b.ts_ins,'
      '       b.ts_upd,'
      '       b.usr_ins,'
      '       b.usr_upd'
      'from buc_merni_mesta b'
      'inner join mat_mesto m on m.id = b.mesto'
      'left outer join mat_opstina o on o.id = m.opstina'
      'order by b.id')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 32
    object tblMerniMestaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblMerniMestaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaSKRATEN_NAZIV: TFIBStringField
      DisplayLabel = #1057#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074
      FieldName = 'SKRATEN_NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblMerniMestaMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaOPSTINA_NAZIV: TFIBStringField
      DisplayLabel = #1054#1087#1096#1090#1080#1085#1072
      FieldName = 'OPSTINA_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblMerniMestaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblMerniMestaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMerniMestaSTEPEN: TFIBSmallIntField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072' '#1086#1076' '#1073#1091#1095#1072#1074#1072
      FieldName = 'STEPEN'
    end
    object tblMerniMestaNIVO_BUCAVA_D: TFIBBCDField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1086' '#1085#1080#1074#1086' '#1085#1072' '#1073#1091#1095#1072#1074#1072' - '#1044#1077#1085
      FieldName = 'NIVO_BUCAVA_D'
      Size = 1
    end
    object tblMerniMestaNIVO_BUCAVA_V: TFIBBCDField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1086' '#1085#1080#1074#1086' '#1085#1072' '#1073#1091#1095#1072#1074#1072' - '#1042#1077#1095#1077#1088
      FieldName = 'NIVO_BUCAVA_V'
      Size = 2
    end
    object tblMerniMestaNIVO_BUCAVA_N: TFIBBCDField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1086' '#1085#1080#1074#1086' '#1085#1072' '#1073#1091#1095#1072#1074#1072' - '#1053#1086#1116
      FieldName = 'NIVO_BUCAVA_N'
      Size = 2
    end
  end
  object dsMerniMesta: TDataSource
    DataSet = tblMerniMesta
    Left = 144
    Top = 32
  end
  object tblRezultati: TpFIBDataSet
    DeleteSQL.Strings = (
      ''
      '    ')
    SelectSQL.Strings = (
      ''
      'select br.id,'
      '       br.godina,'
      '       br.broj,'
      '       br.merno_mesto,'
      '       br.red_br_den,'
      '       br.datum,'
      '       br.vreme,'
      '       bmm.naziv as merno_mesto_naziv,'
      '       br.custom1,'
      '       br.custom2,'
      '       br.custom3,'
      '       br.ts_ins,'
      '       br.ts_upd,'
      '       br.usr_ins,'
      '       br.usr_upd'
      'from buc_rezultati br             '
      'inner join buc_merni_mesta bmm on bmm.id = br.merno_mesto'
      
        'where br.godina like :godina and br.merno_mesto like :merno_mest' +
        'o'
      
        '      and ((br.datum = :datum and :param_datum = 1) or (:param_d' +
        'atum = 0))')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 112
    object tblRezultatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRezultatiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblRezultatiMERNO_MESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1088#1085#1086' '#1084#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MERNO_MESTO'
    end
    object tblRezultatiRED_BR_DEN: TFIBIntegerField
      DisplayLabel = #1056#1077#1076#1077#1085' '#1041#1088'. ('#1076#1085#1077#1074#1085#1086')'
      FieldName = 'RED_BR_DEN'
    end
    object tblRezultatiMERNO_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'MERNO_MESTO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRezultatiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRezultatiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblRezultatiVREME: TFIBTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
      DisplayFormat = 'hh:mm'
    end
    object tblRezultatiBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
  end
  object dsRezultati: TDataSource
    DataSet = tblRezultati
    Left = 144
    Top = 112
  end
  object tblRezultatiStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BUC_REZULTATI_STAVKI'
      'SET '
      '    VREDNOST = :VREDNOST,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select brs.id,'
      '       brs.rezultati_id,'
      '       brs.vrednost,'
      '       brs.reden_broj,'
      '       brs.custom1,'
      '       brs.custom2,'
      '       brs.custom3,'
      '       brs.ts_ins,'
      '       brs.ts_upd,'
      '       brs.usr_ins,'
      '       brs.usr_upd'
      'from buc_rezultati_stavki brs'
      'where(  brs.rezultati_id = :mas_id'
      '     ) and (     BRS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select brs.id,'
      '       brs.rezultati_id,'
      '       brs.vrednost,'
      '       brs.reden_broj,'
      '       brs.custom1,'
      '       brs.custom2,'
      '       brs.custom3,'
      '       brs.ts_ins,'
      '       brs.ts_upd,'
      '       brs.usr_ins,'
      '       brs.usr_upd'
      'from buc_rezultati_stavki brs'
      'where brs.rezultati_id = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsRezultati
    Left = 48
    Top = 184
    object tblRezultatiStavkiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRezultatiStavkiREZULTATI_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1077#1079#1091#1083#1090#1072#1090
      FieldName = 'REZULTATI_ID'
    end
    object tblRezultatiStavkiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiStavkiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiStavkiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblRezultatiStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblRezultatiStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRezultatiStavkiREDEN_BROJ: TFIBSmallIntField
      DisplayLabel = #1056#1077#1076#1077#1085' '#1041#1088#1086#1112
      FieldName = 'REDEN_BROJ'
    end
    object tblRezultatiStavkiVREDNOST: TFIBBCDField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'VREDNOST'
      Size = 2
    end
  end
  object dsRezultatiStavki: TDataSource
    DataSet = tblRezultatiStavki
    Left = 144
    Top = 184
  end
  object PROC_BUC_VNESI_REZULTAT: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_BUC_VNESI_REZULTAT (?GODINA, ?DATUM, ?MER' +
        'NO_MESTO)')
    StoredProcName = 'PROC_BUC_VNESI_REZULTAT'
    Left = 440
    Top = 96
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 440
    Top = 32
  end
  object qDELETE_REZULTAT: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    BUC_REZULTATI'
      'WHERE'
      '    BROJ = :BROJ and GODINA=:GODINA')
    Left = 440
    Top = 168
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
