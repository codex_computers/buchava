program Buchava;

uses
  Vcl.Forms,
  BuchavaMerki in 'BuchavaMerki.pas' {Form1},
  Utils in '..\Share2010\Utils.pas',
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  Login in '..\Share2010\Login.pas' {frmLogin},
  Master in '..\Share2010\Master.pas' {frmMaster},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  MainUnit in 'MainUnit.pas' {frmMain},
  MerniMesta in 'MerniMesta.pas' {frmMerniMesta},
  RezultatiMerenje in 'RezultatiMerenje.pas' {frmRezultatiMerenja},
  Izvestai in 'Izvestai.pas' {frmIzvestai},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  MK in '..\Share2010\MK.pas' {frmMK};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '����� �� ������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='BUC';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.
