inherited frmMerniMesta: TfrmMerniMesta
  Caption = #1052#1077#1088#1085#1080' '#1084#1077#1089#1090#1072' '#1079#1072' '#1084#1077#1088#1077#1114#1077' '#1085#1072' '#1073#1091#1095#1072#1074#1072
  ClientHeight = 682
  ClientWidth = 931
  ExplicitWidth = 939
  ExplicitHeight = 713
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 931
    Height = 290
    ExplicitWidth = 931
    ExplicitHeight = 290
    inherited cxGrid1: TcxGrid
      Width = 927
      Height = 286
      ExplicitWidth = 927
      ExplicitHeight = 286
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsMerniMesta
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 56
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 173
        end
        object cxGrid1DBTableView1SKRATEN_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SKRATEN_NAZIV'
          Width = 191
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Width = 90
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 164
        end
        object cxGrid1DBTableView1OPSTINA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OPSTINA_NAZIV'
          Width = 146
        end
        object cxGrid1DBTableView1STEPEN: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN'
          Width = 165
        end
        object cxGrid1DBTableView1NIVO_BUCAVA_D: TcxGridDBColumn
          DataBinding.FieldName = 'NIVO_BUCAVA_D'
          Width = 183
        end
        object cxGrid1DBTableView1NIVO_BUCAVA_V: TcxGridDBColumn
          DataBinding.FieldName = 'NIVO_BUCAVA_V'
          Width = 190
        end
        object cxGrid1DBTableView1NIVO_BUCAVA_N: TcxGridDBColumn
          DataBinding.FieldName = 'NIVO_BUCAVA_N'
          Width = 186
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Width = 931
    Height = 243
    ExplicitTop = 416
    ExplicitWidth = 931
    ExplicitHeight = 243
    inherited Label1: TLabel
      Left = 76
      ExplicitLeft = 76
    end
    object Label2: TLabel [1]
      Left = 82
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 31
      Top = 75
      Width = 91
      Height = 13
      Caption = #1057#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel [3]
      Left = 69
      Top = 103
      Width = 53
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel [4]
      Left = 2
      Top = 123
      Width = 120
      Height = 27
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1077#1087#1077#1085' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072' '#1086#1076' '#1073#1091#1095#1072#1074#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 128
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsMerniMesta
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 128
    end
    inherited OtkaziButton: TcxButton
      Left = 664
      Top = 187
      TabOrder = 8
      ExplicitLeft = 664
      ExplicitTop = 187
    end
    inherited ZapisiButton: TcxButton
      Left = 583
      Top = 187
      TabOrder = 7
      ExplicitLeft = 583
      ExplicitTop = 187
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 128
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsMerniMesta
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 617
    end
    object SKRATEN_NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 128
      Top = 72
      BeepOnEnter = False
      DataBinding.DataField = 'SKRATEN_NAZIV'
      DataBinding.DataSource = dm.dsMerniMesta
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 617
    end
    object MESTO_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 172
      Top = 100
      BeepOnEnter = False
      DataBinding.DataField = 'MESTO'
      DataBinding.DataSource = dm.dsMerniMesta
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'Naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsMesto
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 573
    end
    object MESTO: TcxDBTextEdit
      Tag = 1
      Left = 128
      Top = 100
      BeepOnEnter = False
      DataBinding.DataField = 'MESTO'
      DataBinding.DataSource = dm.dsMerniMesta
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object STEPEN: TcxDBTextEdit
      Tag = 1
      Left = 128
      Top = 128
      BeepOnEnter = False
      DataBinding.DataField = 'STEPEN'
      DataBinding.DataSource = dm.dsMerniMesta
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxGroupBox1: TcxGroupBox
      Left = 82
      Top = 155
      TabStop = True
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1086' '#1085#1080#1074#1086' '#1085#1072' '#1073#1091#1095#1072#1074#1072' '#1080#1079#1088#1072#1079#1077#1085#1072' '#1074#1086' dBA'
      TabOrder = 6
      Height = 58
      Width = 415
      object Label5: TLabel
        Left = 17
        Top = 28
        Width = 25
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'L'#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label4: TLabel
        Left = 145
        Top = 28
        Width = 25
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'L'#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label7: TLabel
        Left = 281
        Top = 28
        Width = 25
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'L'#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object NIVO_BUCAVA_D: TcxDBTextEdit
        Tag = 1
        Left = 48
        Top = 25
        BeepOnEnter = False
        DataBinding.DataField = 'NIVO_BUCAVA_D'
        DataBinding.DataSource = dm.dsMerniMesta
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object NIVO_BUCAVA_V: TcxDBTextEdit
        Tag = 1
        Left = 176
        Top = 25
        BeepOnEnter = False
        DataBinding.DataField = 'NIVO_BUCAVA_V'
        DataBinding.DataSource = dm.dsMerniMesta
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object NIVO_BUCAVA_N: TcxDBTextEdit
        Tag = 1
        Left = 312
        Top = 25
        BeepOnEnter = False
        DataBinding.DataField = 'NIVO_BUCAVA_N'
        DataBinding.DataSource = dm.dsMerniMesta
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 931
    ExplicitWidth = 931
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 659
    Width = 931
    ExplicitTop = 659
    ExplicitWidth = 931
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 248
  end
  inherited PopupMenu1: TPopupMenu
    Top = 248
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 256
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 256
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41897.646664398150000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
