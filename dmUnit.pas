unit dmUnit;

interface

uses
//  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet,
//  FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, pFIBStoredProc;

  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, Variants, Controls, FIBDatabase, pFIBDatabase, pFIBStoredProc,
  frxClass, frxDBSet, cxCustomPivotGrid, cxTL, cxGridCardView,
  cxGridBandedTableView, cxStyles, cxGridTableView, cxClasses;

type
  Tdm = class(TDataModule)
    tblMerniMesta: TpFIBDataSet;
    dsMerniMesta: TDataSource;
    tblMerniMestaID: TFIBIntegerField;
    tblMerniMestaNAZIV: TFIBStringField;
    tblMerniMestaSKRATEN_NAZIV: TFIBStringField;
    tblMerniMestaMESTO: TFIBIntegerField;
    tblMerniMestaMESTO_NAZIV: TFIBStringField;
    tblMerniMestaOPSTINA_NAZIV: TFIBStringField;
    tblMerniMestaCUSTOM1: TFIBStringField;
    tblMerniMestaCUSTOM2: TFIBStringField;
    tblMerniMestaCUSTOM3: TFIBStringField;
    tblMerniMestaTS_INS: TFIBDateTimeField;
    tblMerniMestaTS_UPD: TFIBDateTimeField;
    tblMerniMestaUSR_INS: TFIBStringField;
    tblMerniMestaUSR_UPD: TFIBStringField;
    tblRezultati: TpFIBDataSet;
    dsRezultati: TDataSource;
    tblRezultatiID: TFIBIntegerField;
    tblRezultatiGODINA: TFIBIntegerField;
    tblRezultatiMERNO_MESTO: TFIBIntegerField;
    tblRezultatiRED_BR_DEN: TFIBIntegerField;
    tblRezultatiMERNO_MESTO_NAZIV: TFIBStringField;
    tblRezultatiCUSTOM1: TFIBStringField;
    tblRezultatiCUSTOM2: TFIBStringField;
    tblRezultatiCUSTOM3: TFIBStringField;
    tblRezultatiTS_INS: TFIBDateTimeField;
    tblRezultatiTS_UPD: TFIBDateTimeField;
    tblRezultatiUSR_INS: TFIBStringField;
    tblRezultatiUSR_UPD: TFIBStringField;
    tblRezultatiDATUM: TFIBDateField;
    tblRezultatiVREME: TFIBTimeField;
    tblRezultatiStavki: TpFIBDataSet;
    dsRezultatiStavki: TDataSource;
    tblRezultatiStavkiID: TFIBIntegerField;
    tblRezultatiStavkiREZULTATI_ID: TFIBIntegerField;
    tblRezultatiStavkiCUSTOM1: TFIBStringField;
    tblRezultatiStavkiCUSTOM2: TFIBStringField;
    tblRezultatiStavkiCUSTOM3: TFIBStringField;
    tblRezultatiStavkiTS_INS: TFIBDateTimeField;
    tblRezultatiStavkiTS_UPD: TFIBDateTimeField;
    tblRezultatiStavkiUSR_INS: TFIBStringField;
    tblRezultatiStavkiUSR_UPD: TFIBStringField;
    tblRezultatiStavkiREDEN_BROJ: TFIBSmallIntField;
    PROC_BUC_VNESI_REZULTAT: TpFIBStoredProc;
    TransakcijaP: TpFIBTransaction;
    tblMerniMestaSTEPEN: TFIBSmallIntField;
    tblRezultatiStavkiVREDNOST: TFIBBCDField;
    tblRezultatiBROJ: TFIBIntegerField;
    qDELETE_REZULTAT: TpFIBQuery;
    tblMerniMestaNIVO_BUCAVA_D: TFIBBCDField;
    tblMerniMestaNIVO_BUCAVA_V: TFIBBCDField;
    tblMerniMestaNIVO_BUCAVA_N: TFIBBCDField;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    function insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6, tag : Variant):Variant;
    procedure tblRezultatiStavkiVREDNOSTSetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmKonekcija, DaNe;

{$R *.dfm}

procedure Tdm.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;
function Tdm.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6, tag : Variant):Variant;
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
         ret := Proc.ParamByName(tag).Value;
         result :=ret;
    end;
procedure Tdm.tblRezultatiStavkiVREDNOSTSetText(Sender: TField;
  const Text: string);
begin
     tblRezultatiStavki.Edit;
     tblRezultatiStavkiVREDNOST.Value:=StrToInt(Text);
     tblRezultatiStavki.Post;
end;

end.
